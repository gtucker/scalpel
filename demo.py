#!/usr/bin/env python3

"""
Copyright (C) 2019 Collabora Limited
Author: Guillaume Tucker <guillaume.tucker@collabora.com>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Corporation nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import argparse
import datetime
import json
import os
import subprocess
import sys

import scalpel.bisect
import tests.simulator


def shell_cmd(cmd):
    return subprocess.check_output(cmd, shell=True)


def call(cmd):
    return subprocess.call(cmd, shell=True)


def parse_result(data):
    res_type, res_value = data.decode().split('\n')[:2]
    if res_type == 'float':
        res_value = float(res_value)
    return res_value

def get_results(scenario):
    json_file = '.'.join([scenario, 'json'])
    if os.path.exists(json_file):
        print("Reading results from {}".format(json_file))
        with open(json_file) as f:
            results = json.load(f)
    else:
        if os.path.exists('scalpel/__init__.py'):
            print("Refusing to run from within the scalpel repository.")
            print("Please create a separate directory and run from there.")
            return None
        print("Creating scenario {}".format(scenario))
        sc = tests.simulator.choices[scenario]
        sc.create()
        results = sc.results
        print(results)
        print("Saving results in {}".format(json_file))
        with open(json_file, 'w') as f:
            json.dump(results, f, indent="  ")
    return results


def main(args):
    results = get_results(args.scenario)
    if not results:
        return False
    print("Results:")
    for res in results:
        print(res)

    print("Initialising bisection")
    bisect = scalpel.bisect.GitBisection('.')

    print("Processing results")
    commit_results = [(c['revision'], c['results']) for c in results]
    loop = 0
    while bisect.iterate(commit_results):
        loop += 1
        if loop == 7:
            print("ABORTING -- too many loops")
            break
        print("\n---- Loop #{} ----".format(loop))
        if not bisect.tasks:
            break
        for task in bisect.tasks:
            print("Task:")
            for k, v in task.items():
                print("  {:16} {}".format(k, v))
            tests.simulator.Git.checkout(task['revision'])
            results = list(
                parse_result(shell_cmd("./{}".format(task['test'])))
                for i in range(task['runs'])
            )
            commit_results.append((
                task['revision'],
                {task['test']: results}
            ))

    print("\nHistory:")
    call("git log --oneline --decorate=no master")

    print("\nCommits found:")
    for found in bisect.found:
        print("* {}".format(found))

    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser("""
Create a git tree and run a bisection
""")
    parser.add_argument("scenario", choices=tests.simulator.choices.keys(),
                        help="Name of the scenario to use")
    parser.add_argument("--branch",
                        help="Base branch name to use")
    args = parser.parse_args(sys.argv[1:])
    res = main(args)
    sys.exit(0 if res is True else 1)
