"""
Copyright (C) 2015 Intel Corporation

Copyright (C) 2019 Collabora Limited
Author: Guillaume Tucker <guillaume.tucker@collabora.com>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Corporation nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from datetime import datetime
from scipy import stats
import math
import numpy
import pygit2

from . import scm


class Result:
    TYPE_STRING = 1
    TYPE_FLOAT = 2

    def __init__(self, name, runs, commit):
        self._name = name
        self._runs = runs
        self._unit = None
        self._commit = commit

    @classmethod
    def auto(cls, name, runs, commit):
        std_types = {
            str: StringResult,
            float: FloatResult,
        }
        values_cls = set(r.__class__ for r in runs)
        if len(values_cls) != 1:
            raise Exception("Invalid result value types")
        result_cls = std_types[values_cls.pop()]
        return result_cls(name, runs, commit)

    @property
    def name(self):
        return self._name

    @property
    def runs(self):
        return list(self._runs)

    @property
    def commit(self):
        return self._commit

    @classmethod
    def value_type(cls):
        return cls._value_type

    @classmethod
    def severity(cls, diff, confidence):
        raise NotImplementedError("severity() required")

    def to_set(self):
        return set(self._runs)

    def check_variance(self, max_variance):
        """Return True if the variance is too high"""
        raise NotImplementedError("check_variance() required")

    def compare(self, other):
        """Return difference to the other and confidence in normal distribution
        """
        raise NotImplementedError("compare() required")


class StringResult(Result):
    _value_type = Result.TYPE_STRING

    @classmethod
    def severity(cls, diff, confidence):
        return 1

    def check_variance(self, max_variance):
        return len(self.to_set()) < 2

    def compare(self, other):
        return (0, 0) if (self.to_set() == other.to_set()) else (1, 1)


class FloatResult(Result):
    _value_type = Result.TYPE_FLOAT

    def __init__(self, *args, alpha=0.95, equal_var=True, **kw):
        super().__init__(*args, **kw)
        self._alpha = 0.95
        self._equal_var = equal_var
        self._data = None
        self._invalidate_cache()

    @classmethod
    def severity(cls, diff, confidence):
        return min(abs(diff), 1) * confidence

    @property
    def data(self):
        if self._data is None:
            self._data = numpy.array(self._runs)
        return self._data

    def _invalidate_cache(self):
        self._mean = None
        self._mean_simple = None
        self._std = None

    def _compute_stats(self):
        if self._mean is not None and self._std is not None:
            return

        data = self.data
        if len(data) > 1:
            self._mean, _, self._std = stats.bayes_mvs(data, self._alpha)
            if numpy.math.isnan(self._mean[0]):
                self._mean = self._stat_tuple(data[0])
                self._std = self._stat_tuple(0)
        else:
            value = data[0] if len(data) else 0
            self._mean = self._stat_tuple(value)
            self._std = self._stat_tuple(float('inf'))

    def _stat_tuple(self, x):
        return (x, (x, x))

    def check_variance(self, max_variance):
        self._compute_stats()
        margin = (
            (self._mean[1][1] - self._mean[1][0]) / 2 / self._mean[0]
            if self._mean[0] > 0 else 0
        )
        return margin < max_variance

    def mean(self):
        if self._mean is not None:
            return self._mean[0]
        if self._mean_simple is None:
            data = self.data
            self._mean_simple = (sum(data) / len(data) if len(data) > 0 else 0)
        return self._mean_simple

    def compare(self, other):
        if self.data.var() == other.data.var() == 0:
            p = 1 if self.data[0] == other.data[0] else 0
        else:
            _, p = stats.ttest_ind(other.data, self.data,
                                   equal_var=self._equal_var)
        if other.mean() != 0:
            diff = abs(self.mean() - other.mean()) / other.mean()
        elif self.mean() == other.mean():
            diff = 0
        else:
            diff = float('inf')

        return diff, 1 - p


class Commit:

    def __init__(self, repo, sha1, results):
        self._obj = repo.get(sha1)
        self._sha1 = sha1
        self._commit_time = None
        self._results = {
            k: Result.auto(k, v, self) for k, v in results.items()
        }
        self._geom_mean_cache = -1
        self._oldness = None

    def __eq__(x, y):
        return y is not None and x.sha1 == y.sha1

    def __hash__(self):
        return hash(self.sha1)

    @property
    def sha1(self):
        return self._sha1

    @property
    def results(self):
        return self._results

    @property
    def commit_time(self):
        if self._commit_time is None:
            self._commit_time = datetime.fromtimestamp(self._obj.commit_time)
        return self._commit_time

    @property
    def oldness(self):
        if self._oldness is None:
            raise Exception("Commit oldness not set")
        return self._oldness

    def set_oldness(self, now, max_delta):
        if self._oldness is not None:
            raise Exception("Commit oldness already set")
        delta = now - self.commit_time
        self._oldness = max(0.1, min(1, delta / max_delta))

    def test_names(self):
        return set(self._results.keys())


# ToDo: Find a better name as this is more than just bisection but
# orchestration to schedule more runs when needed etc...
class Bisection:

    # ToDo: load settings from somewhere
    def __init__(self, graph,
                 max_variance=0.025,
                 min_diff_confidence=0.99,
                 smallest_perf_change=0.005,
                 min_run_count=3,
                 max_extra_runs=20,
                 max_runs=301):
        self._graph = graph
        self._max_variance = max_variance
        self._min_diff_confidence = min_diff_confidence
        self._smallest_perf_change = smallest_perf_change
        self._min_run_count = min_run_count
        self._max_extra_runs = 20
        self._max_runs = max_runs
        self._checked_variance = dict()
        self._tasks = dict()
        self._found = set()

    @property
    def tasks(self):
        return self._tasks.values()

    @property
    def found(self):
        return self._found

    def _update_commits_oldness(self, commits):
        now = datetime.now()
        max_delta = now - commits[0].commit_time
        for c in commits:
            c.set_oldness(now, max_delta)

    def _apply_overlay(self, commits):
        overlay = scm.ResultsDAG(self._graph)
        if len(commits) == 1:
            return overlay
        subg = self._graph.subDAG(list(c.sha1 for c in commits))
        for commit in commits:
            test_names = commit.test_names()
            overlay.set_results(commit.sha1, test_names)
            subg.set_results(commit.sha1, test_names)
        for commit in commits:
            closest = subg.find_closest_nodes_with_results(commit.sha1)
            for other_sha1, res in closest:
                overlay.add_edge(commit.sha1, other_sha1, results=res)
        return overlay

    def _check_variance(self, result):
        return self._checked_variance.setdefault(
            result, result.check_variance(self._max_variance))

    def iterate(self, commits):
        self._update_commits_oldness(commits)
        overlay = self._apply_overlay(commits)
        test_names = set().union(*(c.test_names() for c in commits))
        commits = {c.sha1: c for c in commits}
        sha1_set = set(commits.keys())
        self._tasks = dict()

        for test_name in test_names:
            print("\nResults for {}:".format(test_name))
            leaves = set(sha1_set)
            for node in overlay.nodes():
                if test_name not in overlay.results(node):
                    print("- {} not in {}".format(test_name, node))
                    leaves.discard(node)
                    continue
                print("* node: {}".format(node))
                self._process_node(overlay, node, test_name, leaves, commits)
            if leaves:
                print("Leaves:")
                self._check_base(set(leaves), commits, test_name)

        return bool(self.tasks)

    def _process_node(self, overlay, node, test_name, leaves, commits):
        commit = commits[node]
        after = commit.results[test_name]

        if after is None:
            print("  no after result")
            return

        if not overlay.parents(node):  # debug
            print("  no parents")

        items = []
        parents = set()
        one_similar = False  # don't bisect if at least one parent was similar
        one_inconclusive = False  # don't bisect if not enough runs
        for parent in overlay.parents(node):
            if test_name not in overlay.edge_results(parent, node):
                continue

            leaves.discard(node)
            parents.add(parent)
            parent_commit = commits[parent]
            before = parent_commit.results[test_name]

            # debug
            print("  before: {} {}".format(
                parent_commit.sha1, before.runs))
            print("  after:  {} {}".format(
                commit.sha1, after.runs))

            check_before = self._check_variance(before)
            check_after = self._check_variance(after)
            if not check_before or not check_after:
                print("  variance too high (before: {}, after: {})".format(
                    check_before, check_after))
                continue

            diff, confidence = after.compare(before)
            print("  diff: {}, confidence: {}".format(diff, confidence))
            if confidence < self._min_diff_confidence:
                print("  similar parent")
                one_similar = True
                continue

            if before.value_type() == Result.TYPE_FLOAT:
                if diff > self._smallest_perf_change:
                    items.append((before, after, diff, confidence))
            elif before.value_type() == Result.TYPE_STRING:
                # ToDo: make min_run_count an attribute of the result
                if len(before.runs) < self._min_run_count:
                    self._add_more_runs(before, parent_commit)
                if len(after.runs) < self._min_run_count:
                    self._add_more_runs(after, commit)
                if len(before.runs) >= self._min_run_count and \
                   len(after.runs) >= self._min_run_count:
                    items.append((before, after, diff, confidence))
                else:
                    print("  inconclusive results (missing runs)")
                    one_inconclusive = True

        if not one_similar and not one_inconclusive and items:
            graph = self._graph.walk([node], parents)
            for it in items:
                self._potential_change(graph, *it)

    def _check_base(self, bottom_leaves, commits, test_name):
        leaves = set(bottom_leaves)
        first = leaves.pop()
        if first is None:
            print("no first leave...")
            return
        print("  * first leaf: {}".format(first))
        first_result = commits[first].results.get(test_name)
        if first_result is None:
            print("no first result...")
            return
        print("    results: {}".format(first_result.runs))

        for leaf in leaves:
            print("  * leaf: {}".format(leaf))
            commit = commits[leaf]
            result = commit.results.get(test_name)
            if result is None:
                print("no leaf result...")
                continue
            print("    results: {}".format(result.runs))
            diff, confidence = first_result.compare(result)
            print("    diff: {}, confidence: {}".format(diff, confidence))
            if confidence >= self._min_diff_confidence:
                print("    DIVERGING RESULTS")
                merge_base = self._graph.merge_base(bottom_leaves)
                print("    merge base: {}".format(merge_base))
                self._add_task(merge_base, test_name, 1, len(result.runs))
                return

    def _potential_change(self, graph, before, after, diff, confidence):
        if before is after:
            return
        bscores = graph.bisecting_scores(after.commit.sha1)
        if not bscores:
            return
        middle = bscores[0][0]
        if middle is None:
            return
        if len(bscores) == 1:
            self._add_found(middle, before.name)
            return
        prio = 0.75  # ToDo: parameter
        severity = before.severity(diff, confidence)
        weight = 1 - (before.commit.oldness + after.commit.oldness) / 2
        score = prio * weight * severity
        n_runs = (len(before.runs) + len(after.runs)) / 2
        self._add_task(middle, before.name, score, n_runs)

    def _add_more_runs(self, result, commit):
        missing_runs = max(1, self._min_run_count - len(result.runs))
        extra_runs = min(self._max_runs, missing_runs)
        n_runs = len(result.runs) + extra_runs
        if n_runs > self._max_runs:
            n_runs = self._max_runs = len(result.runs)
            if runs == 0:
                return
        prio = 1
        severity = min(missing_runs / len(result.runs), 1)
        weight = 1 - commit.oldness
        score = prio * weight * severity
        self._add_task(commit.sha1, result.name, score, n_runs)

    def _add_task(self, sha1, test_name, score, n_runs):
        print("  add task: {}".format(sha1))
        key = (sha1, test_name)
        if key in self._found:
            print("  (already found)")
            return
        n_runs = math.ceil(n_runs)
        prev = self._tasks.get(key)
        if prev:
            score = max(prev['score'], score)
            n_runs = max(prev['runs'], n_runs)
        self._tasks[key] = {
            'revision': sha1,
            'test': test_name,
            'score': score,
            'runs': n_runs,
        }

    def _add_found(self, sha1, test_name):
        print("  FOUND: {}".format(sha1))
        key = (sha1, test_name)
        if key in self._found:
            print("  (already found)")
            return
        self._found.add(key)  # ToDo: add parent info with results regression


class GitBisection(Bisection):

    def __init__(self, repo_path, **kw):
        self._repo = pygit2.Repository(repo_path)
        graph = scm.GitRepo(repo_path)
        super().__init__(graph, **kw)

    def iterate(self, data):
        commits = sorted(
            (Commit(self._repo, sha1, results) for sha1, results in data),
            key=lambda c: c.commit_time
        )
        return super().iterate(commits)
