# Scalpel: a generic bisection tool

## Overview

This is a proof-of-concept project to make EzBench's bisection features more
generic and usable in any project.

It contains a `demo.py` script which can be used to showcase how this works,
from outside of the `scalpel` repository as it creates its own git history:

```sh
mkdir test-repo
cd test-repo
git init
../scalpel/scalpel.py demo bool
```

This will print a bunch of debug info; essentially it will create a git history
and store some initial test results in a JSON file.  It will then create a
`scalpel.bisect.GitBisection` object to find the commit that introduced the
problem.  It should eventually show something like this:

```
History:
d2632283278c master.5         foo  fail fail fail Reported
44028c1a71ca master.4         foo  fail fail fail
bb5cb05e280f master.3         foo  pass pass pass
069beb4fdf0c master.2         foo  pass pass pass Reported
64d8a6b46d38 master.1         foo  pass pass pass Reported

Commits found:
* ('44028c1a71ca25947b4dd9d357d75b3f06b2b5a2', 'foo')
```

The commits with `Reported` in their subject means that they were part of the
initial input for the bisection.  The list of commits found shows the revisions
for which some test results started to change.  In this case, `foo` started
failing at commit `44028c1a71ca`.

## A few more details

It's possible to run the bisection in an existing git tree rather than recreate
one every time.  At the end of the example above, you can run it again with
this command:

```
../scalpel/demo.py bool
```

The `bool.json` file contains the initial test results provided, then the
bisection will require intermediate revisions to be tested for each test case
until each commit that introduced a change in the results has been identified.

There are several scenarios that can be used to demonstrate how the bisection
works (`bool` as in the example above, then `diverge` which requires a merge
base to be found first...).  Test results can be either strings (pass/fail
typically) or floating point numbers.  The `demo.py` script iterates until the
bisection has no more things to run, then the results are printed out.

Each test will be bisected separately, and each iteration will create separate
tasks to run various tests at various revisions as needed.  It's up to the
calling code to decide how to combine them, for example if several tasks are
for the same revision it may be more efficient to run all the associated test
cases together.  This depends on the nature of the test suite and the
scheduling system used to produce the test results.

Here's what bisecting the more complex `diverge` scenario produces:

```
History:
a35e81a85f4c master.9         foo  5.300 5.349 5.262 | bar  1.830 1.846 1.846 Reported
f2d9fdce68bc master.8         foo  5.300 5.343 5.320 | bar  1.830 1.818 1.837
1dd19aaea11a master.7         foo  5.300 5.266 5.317 | bar  1.830 1.836 1.840
4397a95c6a51 master.6         foo  6.200 6.183 6.198 | bar  1.830 1.831 1.841
cb8c76131b27 Merge branch 'issue'
1913f2fbb1b0 master.5         foo  6.200 6.151 6.225 | bar  1.150 1.161 1.139
7526332c820f master.4         foo  6.200 6.197 6.220 | bar  1.150 1.144 1.154 Reported
e9732769d500 master.3         foo  5.100 5.107 5.093 | bar  1.150 1.149 1.151
6a02f9c86120 master.issue.3   foo  5.100 5.072 5.147 | bar  1.830 1.829 1.825
0e6ef1c559af master.issue.2   foo  5.100 5.130 5.133 | bar  1.830 1.824 1.817 Reported
85a814796514 master.issue.1   foo  5.100 5.073 5.060 | bar  1.830 1.834 1.839
3b6875a6d0a0 master.2         foo  5.100 5.147 5.146 | bar  1.150 1.144 1.151
cf41748b3d2f master.1         foo  5.100 5.135 5.126 | bar  1.150 1.142 1.158

Commits found:
* ('7526332c820fe3c221169cc7a44c6c65ce2284d5', 'foo')
* ('1dd19aaea11a494427175f02e0a47c3e8c4a7251', 'foo')
* ('85a814796514a18a5bd8803aa0016f406f3bb132', 'bar')
```

In this example, with the 3 commits initially reported on 2 different branches
(`master` and `issue`), the bisection had to find a common ancestor first and
then determine each transition when the results changed independently for `foo`
and `bar`.  **Neat!**

## Using Scalpel with EzBench

The objective is to come up with a generic Python package that can be used by
EzBench among other projects.  It should also still be possible to use
specialised features that may not fit easily in a generic tool, potentially
image pixel comparision.  So it would seem like a good approach to start
retrofitting EzBench to make it use Scalpel while at the early stages to
guarantee it stays compatible.

## Using Scalpel with KernelCI

Another project where bisection matters a lot is KernelCI.  At the moment, only
simple bisections can be run using `git bisect`.  This works well with boot
tests as they provide a single pass/fail result per revision, or with single
test cases.  But it is not practical for test suites that have many test
results likely to fail at different revisions, and result types that are not
just pass/fail.  So Scalpel is also a potential solution for this.

While EzBench typically relies on a server running to keep some information in
memory, other use-cases may not be compatible with this approach.  For example,
one limitation is that the Bisection object is being kept in between iterations
with the revision graph imported from git objects.  One possible approach to
overcome this is to append extra results found between each iteration to the
JSON results file and recreate the object in each loop.  There may however be
some performance issue if many git object have to be read again every time.

Other than that, it should in principle already be possbile to start
experimenting with Scalpel in KernelCI.

## What's next?

Some things were simplified a bit when making this proof-of-concept, so another
round of hacking should be done to add these back if applicable.  In
particular, the confidence margin calculation and image comparison were
dropped.

Then it would be good to have some standalone EzBench unit tests and start a
branch to make it use Scalpel while verifying the same results are still being
produced.  This may even be done using Gitlab CI in the main EzBench
repository.

Meanwhile, the KernelCI bisection can also be modified on a branch to start
experimenting with Scalpel.  Initially, the same bisections can be run with the
current implementation and Scalpel in parallel to verify in practice that they
produce the same results (boot and single test cases).  Then some more complex
scenarios can be handcrafted to evaluate how this would work with multiple test
cases of different types, and with real test suites too.  This can be run on
staging.kernelci.org to not have any impact on the production bisection
results.
