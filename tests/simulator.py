"""
Copyright (c) 2019, Collabora Limited

Authors: Guillaume Tucker <guillaume.tucker@collabora.com>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Corporation nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os
import random
import stat
import subprocess
import time

# Terminal colors...
COLORS = {
    'green': '\033[92m',
    'yellow': '\033[93m',
    'red': '\033[91m',
    'blue': '\033[94m',
    'clear': '\033[0m',
}


def print_color(color, msg):
    print(''.join([COLORS[color], msg, COLORS['clear']]))


class Git:

    @classmethod
    def cmd(cls, cmd):
        print_color('yellow', cmd)
        subprocess.call(cmd, shell=True)

    @classmethod
    def start_branch(cls, branch):
        cls.cmd("git checkout -b {}".format(branch))

    @classmethod
    def checkout(cls, rev):
        cls.cmd("git checkout -q {}".format(rev))

    @classmethod
    def add(cls, path):
        cls.cmd("git add {}".format(path))

    @classmethod
    def commit(cls, msg):
        cls.cmd("git commit -am \"{}\"".format(msg))
        time.sleep(1)  # Needed to have a different timestamp in each commit...

    @classmethod
    def merge(cls, branch):
        cls.cmd("git merge {} -Xtheirs --no-edit".format(branch))

    @classmethod
    def head(cls):
        head = subprocess.check_output(
            "git log --pretty=format:%H -n1", shell=True)
        return head.strip().decode()


class Test:

    def __init__(self, result):
        self._results = [result] * 3  # fake number of runs...

    @property
    def results(self):
        return self._results

    def results_str(self):
        return ' '.join(str(r) for r in self.results)

    def run(self, name):
        raise NotImplementedError("Test.run() is required")


class Bool(Test):

    def __init__(self, result, *args, **kw):
        result_str = "pass" if result is True else "fail"
        super(Bool, self).__init__(result_str, *args, **kw)

    def run(self, px, name):
        with open(name, 'w') as f:
            f.write("# {}: {}\n".format(px, self.results_str()))
            f.write("echo str\n")
            f.write("echo {}\n".format(self._results[0]))
        os.chmod(name, 0o755)
        Git.add(name)


class Float(Test):
    seed = 0

    def __init__(self, result, *args, **kw):
        random.seed(Float.seed)
        Float.seed += 1
        res = float(result)
        noise1, noise2 = (res * random.uniform(0.99, 1.01) for i in range(2))
        self._results = [res, noise1, noise2]

    def results_str(self):
        return ' '.join("{:.3f}".format(r) for r in self.results)

    def run(self, px, name):
        with open(name, 'w') as f:
            f.write("# {}: {}\n".format(px, self.results_str()))
            f.write("echo float\n")
            f.write("echo {}\n".format(self.results[0]))
        os.chmod(name, 0o755)
        Git.add(name)


class Step:

    def create(self, branch):
        raise NotImplementedError("Step.create() is required")


class Tests(Step):

    def __init__(self, tests, report=False):
        self._tests = tests
        self._report = bool(report)

    @property
    def report(self):
        return self._report

    def create(self, branch):
        px = '.'.join([branch.px, str(branch.index())])
        results = {}
        results_str = {}
        for name, test in self._tests.items():
            test.run(px, name)
            results[name] = test.results
            results_str[name] = test.results_str()
        results_str = ' | '.join(['{:4} {:8}'.format(name, result)
                                  for name, result in results_str.items()])
        msg_str = "{:16} {} {}".format(px, results_str,
                                       "Reported" if self.report else "")
        Git.commit(msg_str)
        if self.report:
            return {
                'revision': Git.head(),
                'px': px,
                'results': results,
            }


class Merge(Step):

    def __init__(self, branch):
        self._branch = branch

    def create(self, branch):
        Git.merge(self._branch)


class Branch:

    def __init__(self, name, steps):
        self._name = name
        self._px = name
        self._steps = steps
        self._n = 0
        self._results = list()

    @property
    def name(self):
        return self._name

    @property
    def px(self):
        return self._px

    @property
    def results(self):
        return self._results

    def index(self):
        self._n += 1
        return self._n

    def create(self, parent=None):
        if parent:
            self._px = '.'.join([parent._px, self._px])
        Git.start_branch(self.name)
        for step in self._steps:
            res = step.create(self)
            if res:
                if isinstance(res, list):
                    self._results.extend(res)
                else:
                    self._results.append(res)
        if parent:
            Git.checkout(parent.name)
        return self._results


choices = {
    'bool': Branch("master", [
        Tests({'foo': Bool(True)}, report=True),
        Tests({'foo': Bool(True)}, report=True),
        Tests({'foo': Bool(True)}),
        Tests({'foo': Bool(False)}),
        Tests({'foo': Bool(False)}, report=True),
    ]),
    'diverge': Branch("master", [
        Tests({'foo': Float(5.1), 'bar': Float(1.15)}),
        Tests({'foo': Float(5.1), 'bar': Float(1.15)}),
        Branch("issue", [
            Tests({'foo': Float(5.1), 'bar': Float(1.83)}),
            Tests({'foo': Float(5.1), 'bar': Float(1.83)}, report=True),
            Tests({'foo': Float(5.1), 'bar': Float(1.83)}),
        ]),
        Tests({'foo': Float(5.1), 'bar': Float(1.15)}),
        Tests({'foo': Float(6.2), 'bar': Float(1.15)}, report=True),
        Tests({'foo': Float(6.2), 'bar': Float(1.15)}),
        Merge("issue"),
        Tests({'foo': Float(6.2), 'bar': Float(1.83)}),
        Tests({'foo': Float(5.3), 'bar': Float(1.83)}),
        Tests({'foo': Float(5.3), 'bar': Float(1.83)}),
        Tests({'foo': Float(5.3), 'bar': Float(1.83)}, report=True),
    ]),
}
